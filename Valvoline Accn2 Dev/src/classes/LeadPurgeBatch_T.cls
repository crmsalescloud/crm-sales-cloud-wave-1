@isTest
private class   LeadPurgeBatch_T {
    /****************************************************************************
* Test Class LeadPurgeBatch_T
* --------------------------------------------------------------------------
* Responsible for Testing:- LeadPurgeBatch
****************************************************************************/
    private static testMethod void myUnitTest() {
        
        String recTypeName=RecordTypeUtil.VIOC_Franchise_Web_lead;
        Id recTypeId = RecordType_Functions.LookupRecordTypeId(
            recTypeName, 'Lead');
        
        Test.startTest();
        //Test Data
        
        // List<Sobject> ls  = Test.loadData(Lead.sObjectType, 'LeadStaticresource');
         // Lead lrec = TestObjects.newLead('');
        Lead l1 = new Lead();
        l1.RecordTypeId = recTypeId;
        l1.LastName = 'Test1';
        l1.Company = 'Test1';
        l1.LastModifieddate=system.now().addDays(-2095);
        l1.CreatedDate=system.now().addDays(-2100);
        insert l1;
       
        // Execute Tests
        LeadPurgeBatch.startSchedule();
       LeadPurgeBatch.processDelFlagRecords();
        Test.stopTest();
    }
}