/*
********************************************************************************
@ClassName: LeadPurgeBatchHandler
@author: Accenture Offshore Dev
@description: This is helper class for LeadPurgeBatch. Contains differrent methods which requested from LeadPurgeBatch
@Request Id: 545
@Sprint: 1
********************************************************************************
*/


Public Class LeadPurgeBatchHandler
 {
     /***********************************************************
    @ methodName:deleteVIOCLeads
    @ Developer Name: Offshore dev team
    @Description: This method deletes the lead records which are passed to it.
    @Request: 545
    ***********************************************************/
    public static void deleteVIOCLeads(List<Lead> recs)
    {
        try
        {
           delete recs;
        }
        catch(Exception e)
        {
          System.debug(e.getMessage());
        }
        
    }
 
 }