/*
********************************************************************************
@ClassName: LeadPurgeBatch
@author: Accenture Offshore Dev
@description: This class is responsible for purging the bulk number records which are older than 3 years using batch.
              This class is scheduled class which run on montly basis.
@Request Id: 545
@Sprint: 1
********************************************************************************
*/
global with sharing class LeadPurgeBatch implements Database.Batchable<sObject>, Database.Stateful, Schedulable {
  
    /********************
     * General Constants
     ********************/
    public static final String CLASS_NAME = 'LeadPurgeBatch';
    public static final String PROCESS_NAME = AppLogV2.PROCESS_LEADMANAGEMENT;
    public static final String SUBPROCESS_NAME = AppLogV2.SUBPROCESS_LEADMANAGEMENT;
    public static final String CATEGORY_NAME = AppLogV2.LOGCATEGORY_BATCHPROCESS;
    public static final String TASK_NAME = 'Lead Purgue Batch';
    public static final String TASK_NAME_PROCESS_DEL_VIOC_RECORDS = 'Lead Purge Batch - Process Deletion Of VIOC franchise Lead Records';
  
    /*-*-*-*-*-*-*-*-*-*-*-*
     *                     *
     * Schedulable Section *
     *                     *
     *-*-*-*-*-*-*-*-*-*-*-*/

    /* Developer Console Code Samples - Schedulable
    LeadPurgeBatch.startSchedule();
    LeadPurgeBatch.startSchedule('0 0 * * * ?');
    */

    /*************************
     * Consts for Schedulable
     *************************/
    public static final String SCHEDULE_NAME = 'Lead Purgue Batch';
    public static final String DEFAULT_CRON = '0 0 23 28 1/1 ? *';//28th of every month.

    /***********************************
     * Public Interface for Schedulable
     ***********************************/
    global static String startSchedule() {
        return startSchedule(DEFAULT_CRON);
    }

    global static String startSchedule(String cronExpression) {
        String result = '';
        try 
            {
                result = SystemMethods.schedule(SCHEDULE_NAME,cronExpression,new LeadPurgeBatch());
            }
        catch(Exception e)
        {
            result = e.getMessage();
            System.debug(e);
        }
        return result;
    }

    /*****************************
     * Schedulable Implementation 
     *****************************/
    private LeadPurgeBatch() { 
    }

    global void execute(SchedulableContext sc) {
        LeadPurgeBatch.processDelFlagRecords();
    }

    /*-*-*-*-*-*-*-*-*-*-*
     *                   *
     * Batchable Section *
     *                   *
     *-*-*-*-*-*-*-*-*-*-*/

    /* Developer Console Code Samples - Batchable
    LeadPurgeBatch.updateAccountLookups();
    LeadPurgeBatch.submitBatchJob();
    LeadPurgeBatch.submitBatchJob(50);
    LeadPurgeBatch.processDelFlagRecords();
    LeadPurgeBatch.processDelFlagRecords(50);
    */ 

    /***********************
     * Consts for Batchable
     ***********************/
    public static final Integer BATCH_SIZE = 200;
    private static final String PROCESS_DEL_VIOC_FRANCHISE_RECORDS = 'PROCESS_DEL_VIOC_FRANCHISE_RECORDS';

    /***************************
     * Private Vars - Batchable
     ***************************/
    private BatchTool batchTool;
    private Boolean isScheduled = false;
    private String jobType = '';

    /*******************************
     * Constructor(s) for Batchable
     *******************************/
    global LeadPurgeBatch(Boolean isScheduled, String jobType) {
        this.isScheduled = isScheduled;
        this.jobType = jobType;
    }

    /*********************************
     * Public Interface for Batchable
     *********************************/
    

    global static String processDelFlagRecords() {
        return submitBatch(BATCH_SIZE,PROCESS_DEL_VIOC_FRANCHISE_RECORDS);    
    }
    
    /*global static String processDelFlagRecords(Integer batchSize) {
        return submitBatch(batchSize,PROCESS_DEL_VIOC_FRANCHISE_RECORDS); 
    }*/

    /***************************************
     * Private Static Methods for Batchable
     ***************************************/
     
    /***********************************************************
    @ methodName:submitBatch
    @ Developer Name: Offshore dev team
    @Description: This method will check for whether the class is already running or not, if not
                  It initiates a batch process.
    @Request: 545
    ***********************************************************/
    private static String submitBatch(Integer batchSize, String jobType) {
        String msg;
        if (!BatchTool.alreadyRunning('LeadPurgeBatch')) {
            LeadPurgeBatch b = new LeadPurgeBatch(System.isScheduled(),jobType);
            try 
            {
                msg = Database.executeBatch(b, batchSize);
            } catch(Exception e) {msg = e.getMessage();System.debug(e);}
        } 
        else {
            msg = 'Job is already Running';
            AppLogV2 appLog = new AppLogV2(PROCESS_NAME,SUBPROCESS_NAME,CLASS_NAME,'');
            appLog.write(AppLogV2.LOGCATEGORY_BATCHPROCESS,AppLogV2.TASK_BATCHPROCESSSKIPPED,AppLogV2.LOGTYPE_INFO,AppLogV2.TASK_BATCHPROCESSSKIPPED,AppLogV2.TASK_BATCHPROCESSSKIPPED);
        }
        return msg;
    }

    /********************************
     * Private Methods for Batchable
     ********************************/

    /***************************
     * Batchable Implementation
     ***************************/
     
      
    /***********************************************************
    @ methodName:start
    @ Developer Name: Offshore dev team
    @Description: This method  will return Database.Querylocator by querying the lead records which are older than 3 years and record type is "VIOC Franchise Web lead"            
    @Request: 545
    ***********************************************************/
    global Database.Querylocator start(Database.BatchableContext bc) {
        batchTool = new BatchTool(bc.getJobId(), isScheduled, PROCESS_NAME, SUBPROCESS_NAME);
        
        Database.Querylocator query;
        Integer noOfYrs = Integer.valueOf(System.Label.LeadPurgue_YearsOfOldRecs);
        String queryString='SELECT Id FROM Lead  where RecordType.Name=\''+RecordTypeUtil.VIOC_Franchise_Web_lead+'\''+' and LastModifiedDate < LAST_N_YEARS:'+noOfYrs;
        System.debug('###'+queryString);
        if (jobType == PROCESS_DEL_VIOC_FRANCHISE_RECORDS) {
            //query = Database.getQueryLocator([SELECT Id FROM Lead  where RecordType.Name=:RecordTypeUtil.VIOC_Franchise_Web_lead and LastModifiedDate < LAST_N_YEARS:noOfYrs]);
          query= Database.getQueryLocator(queryString);
        }


        return query;
    }

    /***********************************************************
    @ methodName:execute
    @ Developer Name: Offshore dev team
    @Description: This method calls the  deleteVIOCLeads() of   LeadPurgeBatchHandler for each batch and records gets deleted from there.         
    @Request: 545
    ***********************************************************/
    
    global void execute(Database.BatchableContext bc, List<SObject> recsIn){
      
        List<Lead> leadRecs=(List<Lead>)recsIn;
        if(jobType == PROCESS_DEL_VIOC_FRANCHISE_RECORDS)
        {
          LeadPurgeBatchHandler.deleteVIOCLeads(leadRecs);
        }
    }
    
    
    /***********************************************************
    @ methodName:finish
    @ Developer Name: Offshore dev team
    @Description: This method will be called after completing the all batches.
                 It uses AppLogV2 and BatchTool class to create batch info into AppLogV2__c Object and send 
                 a notification to the users based on the entry from the Batch Job details Custom setting entry.
    @Request: 545
    ***********************************************************/
    
    global void finish(Database.BatchableContext bc) {
       if (jobType == PROCESS_DEL_VIOC_FRANCHISE_RECORDS) {
            batchTool.sendNotifications(TASK_NAME_PROCESS_DEL_VIOC_RECORDS);
        }
    }
}